
"""This script pull's data from Desk's API.

this file is almost identicle to deskToBigQueryNPRApp
except it does some specific cleaning
uploads all cases in desk
takes a few hours to complete

Required:
 Working Google BigQuery Service account and JSON Key on machine
(see https://cloud.google.com/bigquery/docs/authentication/ for more information; also Nick DePrey and Demian)
pip install requests, requests_oauthlib, pandas, google-api-helper
pip install --upgrade google-cloud-bigquery
"""

# https://googlecloudplatform.github.io/google-cloud-python/latest/bigquery/usage.html

## How it works ####### !!!!!!!!!!!
# first the script will make an api call to desk. This call can have many pages of results goes over the page limit (500). 
# So we modify the api call  as we go by sorting in chronalogicall order and periodically changing the call to all results after the last result returned
# for each of these pages we then save the data into a csv slowly adding to it 1 page at a time
# next we delete and recreate an empty databse in big query
# finally we read in the csv in chunks and upload its contents one chunk at a time to big query

#it is split into the three parts which are called at the bottom

import requests
import pandas as pd
import numpy as np
import json
import configparser
from requests_oauthlib import OAuth1
import datetime
import time
import pytz
import os
import math
from requests import ConnectionError

connection_timeout = 1000 # seconds

#from google.cloud import bigquery
from googleapi import GoogleApi


#set up credentials in notebook, only need for local env, not in sandbox
#os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="/Users/jgluck/Documents/To Big Query/Midd-d3b73b87b0cf.json"


####################
#   DESK CLASS     #
####################

#this class is used to pull all data from desk and save it to a csv

class Desk():
    '''
    This class includes all the methods necessary to pull data from Desk.com.

    '''
    def __init__(self, sitename, auth):
        self.sitename = sitename
        self.auth = auth

    # INPUT: request_url - endpoint of api to make call to
    def get_data(self, request_url):
        #make call to Desk api
        requests.packages.urllib3.disable_warnings()
        print(request_url)

        start_time = time.time()

        while True:
            try:
                resp = requests.get(self.sitename + request_url, auth=self.auth, verify=False) #verify=false necessary if running off server
                break
            except ConnectionError:
                if time.time() > start_time + connection_timeout:
                    raise Exception('Unable to get updates after {} seconds of ConnectionErrors'.format(connection_timeout))
                else:
                    time.sleep(1) # attempting once every second
            
        return resp.json()

    #As it stands now there are so few cases that it makes sense to just pull all of them each time. Should this
    #this change incorporate the script from pullAllDeskCases to pull based off time
    #(would require some tweaking of update cases)

    #This function goes through every page in the api call, it loads each one and calls get_df to save them in turn to a csv

    # INPUT: request_url - endpoint of api to make call to
    def cycle_pages(self, request_url, fname, fields=None):
        data = self.get_data(request_url)
    
        #need to use count and change url bc you can only read 500 pages per request, this way we change the date to be the last value and count upward
        #count starts at 1 because the first ever call counts as one, with other requests start at 0
        countLimit = 250
        count = 1
        totalCount = 1
        request_url = data['_links']['next']['href']
        print(data['total_entries'])
        first=True

        #this section is reading into the reponse and pulling the data from it, it then calls get df and saves it one chunk at a time, also get the link for the next page if it exists
        while (('_links' in data) and ('_embedded' in data) and (data['_links']['next'] != None)):
            if fields != None:
                try:
                    self.get_df(data['_embedded']['entries'], first, fname, fields=fields)
                except:
                    print('Fields Invalid, pulling default fields: ')
                    self.get_df(data['_embedded']['entries'], first, fname)
            else:
                self.get_df(data['_embedded']['entries'], first, fname)

            if (first):
                first=False

            request_url = request_url.replace("per_page=50&", "per_page=100&")
            request_url = request_url.replace("&per_page=50", "&per_page=100")
            #For unknown reasons, when authenticating via OAuth1, ':' is replaced by '%3A' -- reverses that
            if '%3A' in request_url:
                request_url = request_url.replace('%3A', ':')
            data= self.get_data(request_url)
            #When we reach the count limit we start a new request 
            count = count + 1
            totalCount = totalCount + 1
            print(str(count) + " - " + str(totalCount))
            if count is countLimit:
                # at the count limit we start a new request, the request uses the time of the last entrie in the previous call
                #the call needs to be in epoch format so we convert it and create the new request url
                n = (len(data['_embedded']['entries']) - 1)
                stime = data['_embedded']['entries'][n]['created_at'].replace("T", " ").replace("Z", "")
                ut = time.mktime(datetime.datetime.strptime(stime, "%Y-%m-%d %H:%M:%S").timetuple())
                request_url = "/api/v2/cases/search?since_created_at=" + str(ut)[:-2] + "&sort_direction=des&sort_field=since_created_at"
                count = 0
            else:
                if (data != None and data['_links']['next'] != None):
                    #if it is not at the count limit we change the request ulr to the next page and continue
                    request_url = data['_links']['next']['href']
            print("-----")

        if '_embedded' in data:
            #this is for the LAST call, it will not have a next link but will still have data so we will read it
            if fields != None:
                try:
                    self.get_df(data['_embedded']['entries'], first, fname, fields=fields)
                except:
                    print('Fields Invalid, pulling default fields: ')
                    self.get_df(data['_embedded']['entries'], first, fname)
                    print(str(self.fields))
            else:
                self.get_df(data['_embedded']['entries'], first, fname)
        else:
            print("no _embedded at end")

    #Grabs the JSON data and returns a properly formatted pandas dataframe with only the columns you specify

    # INPUT: data - dataset to select fields from
    # INPUT: fields - columns to be selected from dataset (add more if you need but for the most part this is all we should need)
    # this runs every time you pull a page, save one page of data at a time
    def get_df(self, data, first, fname, fields=['id', 'blurb', 'labels', 'label_ids', 'created_at', 'opened_at', 'resolved_at', 'received_at', 'updated_at','subject', '_links', 'status', 'custom_fields', 'type']):
        self.fields = fields
        json_data = json.dumps(data)

        for chunk in pd.read_json(json_data, chunksize=10000, lines=True):
            c = chunk.T
            c.rename(columns = {list(c)[0]: 'data'}, inplace = True)
            df = pd.DataFrame(c['data'].tolist())
            df_functional = df[:][fields].copy()

            #if this is the first page create the csv and save, if not just save data
            if (first):
                first = False
                with open(fname, 'w') as outfile:
                    df_functional.to_csv(outfile)
            else:
                with open(fname, 'a') as outfile:
                    df_functional.to_csv(outfile)

    

    # #Writes the pandas data frame to a csv file

    # # INPUT: fname - name of csv to save data to
    # def load_csv(self, fname):
    #     print("load")
    #     with open(fname, 'w') as outfile:
    #         self.df.to_csv(outfile, index=False, chunksize=10000)

    #Combines cycle_pages, get_df, and load_csv into one function
    def pull_save_cases(self, request_url=None, fname=None, fields=None):
        try:
            print("Trying to pull cases from Desk.com")
            print(fname)
            self.cycle_pages(request_url, fname, fields=fields)
            print('You have successfully saved')
        except Exception as e: 
            print(e)
            print(e.__class__)


####################################
#   DELETE AND RECREATE DATABASE   #
####################################

#deletes a database and recreates an empty table
def del_recreate_bq(projectId, datasetId, tableId, tableDescription):
    bigquery = GoogleApi('bigquery', 'v2', [
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/devstorage.full_control'
    ])

    #NEED TO UPDATE TO YOUR JSON  KEY
    bigquery.with_service_account_file('Midd-d3b73b87b0cf.json')
    try:
        service_request = bigquery.tables().delete(projectId=projectId, datasetId=datasetId, tableId=tableId)
        service_request.execute()
    except:
        print('No table to delete')

    body = {
      "tableReference":
      {
        "projectId": projectId,
        "datasetId": datasetId,
        "tableId": tableId
      },
      "description": tableDescription
    }

    try:
        service_request = bigquery.tables().insert(projectId=projectId, datasetId=datasetId, body=body)
        service_request.execute()
        print('Success, created table ' + tableId)
    except:
        print('Unable to create table ' + tableId)
    return None

####################################
#   LOAD DATA TO BIGQUERY          #
####################################   

#this section is called last, it pulls the full dataset from the csv created earlier
#it then uploads the data to the bigquery database

#Modified from google documentation

#after a large amount of time i have determined that this is the ideal way to upload a csv to bigquery
def load_data_from_file(dataset_id, table_id, source_file_name):
    pd.set_option('display.max_columns', None) 

    # Instantiates a client
    bigquery_client = bigquery.client.Client()
    dataset_ref = bigquery_client.dataset(dataset_id)
    table_ref = dataset_ref.table(table_id)

    # load clean, and upload the data to bq 10000 at a time
    cc = 0
    chunks = pd.read_csv(source_file_name, chunksize=10000, lineterminator='\n')
    ids = set()

    for chunk in chunks:
        try:
            print(cc)
            cc = cc + 1
            print(len(chunk))
            print(len(ids))

            #just make sure its a real chunk
            if (len(chunk) > 10):

                #remove any duplicates, by checking agains ids, a set that holds every id already uploaded and removing any duplicates
                df = chunk.drop_duplicates(['id', 'blurb', 'created_at'], keep='last')
                df = df[~df['id'].isin(ids)]
                ids.update(df['id'].values)

                #there is a chance this odd column can be added by saving to a csv earlier in the script, so remove it if it has been
                if 'Unnamed: 0' in list(df):
                    df = df.drop('Unnamed: 0', axis=1)
                df.columns = df.columns.str.strip()

                #fill na fields with correct empty type, should match schema below
                values = {'id': "", 'blurb':"",'labels':"[]", 'label_ids':"[]", 'created_at': "", 'opened_at': "", 'resolved_at': "", 'received_at': "", 'updated_at': "", 'subject': "", "_links":"",'status':"", 'custom_fields': "",'type':""}
                df.fillna(value=values)
                # df.replace([np.inf, -np.inf], "")
                # df.id = pd.to_numeric(df['id'], errors='coerce')
                # df = df[np.isfinite(df['id'])]
                df.rename(columns={'labels':'assigned_labels'}, inplace=True)

                # df['id'] = df['id'].apply(pd.to_numeric, errors='coerce').astype(int).dropna()

                #do this incase pandas interprets id as different type, do after dropna, maybe use on other fields if it interprets wrong in the future
                df.id = df.id.astype(str)

                df.blurb = df.blurb.astype(str)
                df.assigned_labels = df.assigned_labels.astype(str)
                df.label_ids = df.label_ids.astype(str)

                df.created_at = pd.to_datetime(df['created_at'], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                df.opened_at = pd.to_datetime(df['opened_at'], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                df.resolved_at = pd.to_datetime(df['resolved_at'], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                df.received_at = pd.to_datetime(df['received_at'], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                df.updated_at = pd.to_datetime(df['updated_at'], format='%Y-%m-%d %H:%M:%S', errors='coerce')

                df.subject = df.subject.astype(str)
                df._links = df._links.astype(str)
                df.status = df.status.astype(str)
                df.custom_fields = df.custom_fields.astype(str)
                df.type = df.type.astype(str)

                #if a row of column labels is in dataframe remove it
                df['id'] = df['id'].map(lambda x: x.strip().lstrip().rstrip())
                df = df.loc[df['id'] != 'id']

                # This example uses CSV, but you can use other formats.
                # See https://cloud.google.com/bigquery/loading-data
                # https://google-cloud-python.readthedocs.io/en/latest/bigquery/generated/google.cloud.bigquery.client.Client.html#google.cloud.bigquery.client.Client
                job_config = bigquery.LoadJobConfig()
                job_config.source_format = 'Parquet'

                #create the schema used, this tell the job what type each column is, this should match the types of the values you used for fill na above
                job_config.schema = [
                        bigquery.SchemaField('id', 'STRING', description="Unique Id of the email"),
                        bigquery.SchemaField('blurb', 'STRING', description="First 250 chars of the body of the email"),
                        bigquery.SchemaField('assigned_labels', 'STRING', description="Categories applied by Audience Relations that describe the email"),
                        bigquery.SchemaField('label_ids', 'STRING', description="Id's in desk for each Label used"),
                        bigquery.SchemaField('created_at', 'TIMESTAMP', description="When the case was created in desk (The email was sent to us)"),
                        bigquery.SchemaField('opened_at', 'TIMESTAMP', description="When the case was first opened by an agent"),
                        bigquery.SchemaField('resolved_at', 'TIMESTAMP', description="When the case was last resolved (If it has been)"),
                        bigquery.SchemaField('received_at', 'TIMESTAMP', description="The last time we recieved something for this case (Can be part of a chain of corespondence)"),
                        bigquery.SchemaField('updated_at', 'TIMESTAMP', description="Categories applied by Audience Relations that describe the email"),
                        bigquery.SchemaField('subject', 'STRING', description="A broad single category for the case (What the user self selected the email was about)"),
                        bigquery.SchemaField('_links', 'STRING', description="Various links in the API that relate to the case (attachments, customer, ect)"),
                        bigquery.SchemaField('status', 'STRING', description="Can be an open or resolved case (Have we dealt with the issue)"),
                        bigquery.SchemaField('custom_fields', 'STRING', description="A list of our (NPR's) custom fields for the case"),
                        bigquery.SchemaField('type', 'STRING', description="Type of case (almost always an email)")
                    ]
                job = bigquery_client.load_table_from_dataframe(
                df, table_ref, job_config=job_config)

                # Waits for job to complete
                while True:
                    try:
                        if (len(df) > 10):
                            job.result()
                        else:
                            break
                    finally:
                        if (job.errors != None):
                            print(job.errors)
                        if (job.error_result != None):
                            print(job.error_result)
                    if job.state == 'DONE':
                        if job.error_result:
                            raise RuntimeError(job.errors)

                        print('Loaded {} rows into {}:{}.'.format(
                        job.output_rows, dataset_id, table_id))
                        break
                    time.sleep(1)
        except Exception as e:
            print("error caught")
            print(e)


######################################
#   FUNCTIONS CALLED BELOW HERE      #
######################################
                                
"""Can take configuration file with data for OAuth1, or else set yourself
If setting yourself, you can just pass in a username and password as such
import getpass
username = input('Enter username:')
password = getpass.getpass()
auth = username, password
"""
sitename = 'https://help.npr.desk.com'
all_data = 'allCases.csv'

#https://docs.python.org/3/library/configparser.html
config = configparser.ConfigParser(allow_no_value=True)
config.read('desk_OAuth1.ini')
auth = OAuth1(config['DESK']['app_key'],
              config['DESK']['app_secret'],
              config['DESK']['oauth_token'],
              config['DESK']['oauth_token_secret'])

##########Change for Dataset#########
projectId = "midd-194719"
datasetId = "hippo"
tableIdAll = 'allCases'
all_desc = "All cases in Desk, almost all are emails"
#####################################

# #Has to Be imported beforehand
from google.cloud import bigquery

##### Grabing the desk cases for the NPR App #######################################################

#create desk client
client = Desk(sitename, auth)

###### Grabing all desk data ##########################################################
#all cases created after nprs founding, so all cases
#need to use sort field and dirrection so values appear in chronological order
#this call uses the client class and its functions cycle_pages() and get_df() and get_data()
client.pull_save_cases(request_url="/api/v2/cases/search?since_created_at=946684800&sort_direction=des&sort_field=since_created_at", fname=all_data)
#Deleteing and recreating the bq tables
del_recreate_bq(projectId, datasetId, tableIdAll, all_desc)
#Loading data from the NPR App and Labels to their tables
load_data_from_file(datasetId, tableIdAll, all_data)







#See https://pandas.pydata.org/pandas-docs/stable/generated/pandas.DataFrame.to_gbq.html



