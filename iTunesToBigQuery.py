

"""
This script pulls iTunes reviews for the NPR News/NPR App (324906251) collected on
http://dev-sandbox.npr.org/dperry/reviews/apps/324906251.json

It requests the saved iTunes reviews in JSON format, turns them into a csv, then saves that CSV file to BigQuery.
Schema for the BigQuery table is automatically detected.

For documentation and code on how the iTunes reviews are grabbed, see https://gitlab.com/perrydc/reviews.


Required libraries:
pip install requests, pandas, google-api-helper
pip install --upgrade google-cloud-bigquery
"""

import requests
import pandas as pd
import json
import datetime as dt

#https://pypi.python.org/pypi/google-api-helper/0.2.2
from googleapi import GoogleApi

from google.cloud import bigquery


#set up credentials in notebook, only need for local env, not in sandbox
#import os
#os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="/Users/jgluck/Documents/To Big Query/Midd-d3b73b87b0cf.json"

""" BIGQUERY
    Make sure that JSON key is in correct location and that bash profile is modified to have the path set.
    Uses two different libraries / python wrappers, one to delete and recreate the table,
    the other to load data with the schema auto-detected
    https://cloud.google.com/bigquery/docs/authentication/
"""

def del_recreate_bq(projectId, datasetId, tableId, tableDescription):
    bigquery = GoogleApi('bigquery', 'v2', [
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/devstorage.full_control'
    ])

    ##CHANGE THE PATH & JSON KEY HERE TO MATCH YOURS
    bigquery.with_service_account_file('Midd-d3b73b87b0cf.json')
    try:
        service_request = bigquery.tables().delete(projectId=projectId, datasetId=datasetId, tableId=tableId)
        service_request.execute()
    except:
        print('No table to delete')         
    body = {
      "tableReference":
      {
        "projectId": projectId,
        "datasetId": datasetId,
        "tableId": tableId
      },
      "description" : tableDescription
    }

    try:
        service_request = bigquery.tables().insert(projectId=projectId, datasetId=datasetId, body=body)
        service_request.execute()
        print('Success, created table ' + tableId)
    except:
        print('Unable to create table ' + tableId)
    return None


def load_data_from_file(dataset_id, table_id, source_file_name):
    
    #this print full datafram if you need it to debug
    #pd.set_option('display.max_columns', None) 

    # Instantiates a client
    bigquery_client = bigquery.client.Client()
    dataset_ref = bigquery_client.dataset(dataset_id)
    table_ref = dataset_ref.table(table_id)

    # load clean, and upload the data to bq 10000 at a time
    for chunk in pd.read_csv(source_file_name, chunksize=10000):
        df = chunk.drop_duplicates()
        if (source_file_name != 'deskCasesLabels.csv'):
            values = {'appVersion': "", 'content':"",'name':"", 'rating':0, 'title': "", 'created_at': ""}
            df.fillna(value=values)

        df.rating = df.rating.astype(int)
        df.created_at = pd.to_datetime(df['created_at'], format='%Y-%m-%d %H:%M:%S')

        # This example uses CSV, but you can use other formats.
        # See https://cloud.google.com/bigquery/loading-data
        # https://google-cloud-python.readthedocs.io/en/latest/bigquery/generated/google.cloud.bigquery.client.Client.html#google.cloud.bigquery.client.Client
        job_config = bigquery.LoadJobConfig()
        job_config.source_format = 'Parquet'
        job_config.schema = [
           bigquery.SchemaField('appVersion', 'STRING', description="Version of the app the review was left for"),
           bigquery.SchemaField('content', 'STRING', description="Message left with the review"),
           bigquery.SchemaField('name', 'STRING', description="Name of account that left the review"),
           bigquery.SchemaField('rating', 'INTEGER', description="Rating left for review (1-5)"),
           bigquery.SchemaField('title', 'STRING', description="Title of the review"),
           bigquery.SchemaField('created_at', 'TIMESTAMP', description="When the review was left")
        ]
        job = bigquery_client.load_table_from_dataframe(
        df, table_ref, job_config=job_config)

         # Waits for job to complete
        while True:
            try:
                job.result()
            finally:
                if (job.errors != 'None'):
                    print(job.errors)
                if (job.error_result != 'None'):
                    print(job.error_result)
            if job.state == 'DONE':
                if job.error_result:
                    raise RuntimeError(job.errors)
                break
            time.sleep(1)

        print('Loaded {} rows into {}:{}.'.format(
        job.output_rows, dataset_id, table_id))


#writes file of itunes reviews combined from multiple sources
def get_news_app_reviews():

    # ITUNES REVIEWS FROM dev-sandbox.npr.org
    resp = requests.get("http://dev-sandbox.npr.org/dperry/reviews/apps/324906251.json")
    df = pd.DataFrame(resp.json()['reviews'])
    df = df.set_index('id')
    df = df.rename(columns={'date': 'created_at'})

    #APPTENTIVE DATA
    apptentive_reviews = json.load(open('archived_npr_news_ios.json', encoding="utf8"))['reviews'] #Back through November to create more even idea of average volume
    df_apptentive= pd.DataFrame(apptentive_reviews)
    #Reconfiguring to match stored iTunes files from dev-sandbox.npr.org
    df_apptentive['store_user_name'].fillna(df_apptentive['store_user_id'], inplace=True) #Older apptentive reviews don't have unique name, creating from unique user id
    df_apptentive = df_apptentive.drop(['store_app_id', 'store_user_id','language'], axis=1)
    df_apptentive.columns = ['appVersion', 'content', 'created_at', 'id', 'rating', 'name', 'title']
    df_apptentive = df_apptentive.set_index('id')

    #COMBINE & DROP DUPLICATES
    combined_df = pd.concat([df_apptentive, df])
    combined_df['content'] = [' '.join(i.split()) for i in combined_df['content']] #Fix spacing problems 
    combined_df.drop_duplicates(subset=['name', 'content', 'title', 'appVersion'], inplace=True) #only gets rid of identical duplicates

    #Reversing the order of the dataframe so that Apptentive Reviews Come first; necessary so that BigQuery AutoDetects the schema correctly
    combined_df = combined_df.sort_values(by="created_at", ascending=False)

    # Writing to CSV
    fname = 'iTunesNewsReviews.csv'
    with open(fname, 'w', encoding="utf8") as outfile:
                combined_df.to_csv(outfile, index=False)
    

#writes file of npr oen reviews
def get_one_reviews():
    # ITUNES REVIEWS FROM dev-sandbox.npr.org
    resp = requests.get("http://dev-sandbox.npr.org/dperry/reviews/apps/874498884.json")
    df = pd.DataFrame(resp.json()['reviews'])
    df = df.set_index('id')
    df = df.rename(columns={'date': 'created_at'})
    df = df.sort_values(by="created_at", ascending=True)
    df = df.dropna()
    # Writing to CSV
    fname = 'iTunesOneReviews.csv'
    with open(fname, 'w', encoding="utf8") as outfile:
                df.to_csv(outfile, index=False, chunksize=1000)
#################################################################

##########Change for Dataset#########
projectId = "midd-194719"
datasetId = "hippo"
tableIdNPRApp = "iTunesNewsReviews"
tableIdNPROneApp = "iTunesOneReviews"
fileNPRApp = "iTunesNewsReviews.csv"
fileNPROneApp = "iTunesOneReviews.csv"
app_desc = "Reviews left in the iTunes store for the NPR app"
one_desc = "Reviews left in the iTunes store for the NPR One app"
#####################################

get_news_app_reviews() 

#Deleting and recreating the bq tables
del_recreate_bq(projectId, datasetId, tableIdNPRApp, app_desc)

#Has to be imported after del_recreate_bq


#Loading data from the NPR App and Labels to their tables
load_data_from_file(datasetId, tableIdNPRApp, fileNPRApp)

get_one_reviews() 

#Deleting and recreating the bq tables
del_recreate_bq(projectId, datasetId, tableIdNPROneApp, one_desc)


#Loading data from the NPR App and Labels to their tables
load_data_from_file(datasetId, tableIdNPROneApp, fileNPROneApp)
