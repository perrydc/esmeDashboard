
"""This script pull's data from Desk's API.

it uploads to three datasets last month, nprapp, and labels

#this script is basically the same as allCasesToBigQuery.py
#that file is a more simplistic version of this and may be better to reference if you woukld like to modify this code for another purpose


Required:
 Working Google BigQuery Service account and JSON Key on machine
(see https://cloud.google.com/bigquery/docs/authentication/ for more information; also Nick DePrey and Demian)
pip install requests, requests_oauthlib, pandas, google-api-helper
pip install --upgrade google-cloud-bigquery
"""

# https://googlecloudplatform.github.io/google-cloud-python/latest/bigquery/usage.html

import requests
import pandas as pd
import numpy as np
import json
import configparser
from requests_oauthlib import OAuth1
import datetime
import time
import pytz
import os
import math
from requests import ConnectionError
import sys

connection_timeout = 1000 # seconds

#from google.cloud import bigquery
from googleapi import GoogleApi


#set up credentials in notebook, only need for local env, not in sandbox
#os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="/Users/jgluck/Documents/To Big Query/Midd-d3b73b87b0cf.json"


####################
#   DESK CLASS     #
####################

#this class is used to pull all data from desk and save it to a csv

## How it works ####### !!!!!!!!!!!
# first the script will make an api call to desk. This call can have many pages of results goes over the page limit (500). 
# So we modify the api call  as we go by sorting in chronalogicall order and periodically changing the call to all results after the last result returned
# for each of these pages we then save the data into a csv slowly adding to it 1 page at a time
# next we delete and recreate an empty databse in big query
# finally we read in the csv in chunks and upload its contents one chunk at a time to big query

#it is split into the three parts which are called at the bottom


class Desk():
    '''
    This class includes all the methods necessary to pull data from Desk.com.

    '''
    def __init__(self, sitename, auth):
        self.sitename = sitename
        self.auth = auth

    # INPUT: request_url - endpoint of api to make call to
    def get_data(self, request_url):
        #make call to Desk api
        requests.packages.urllib3.disable_warnings()
        print(request_url)

        start_time = time.time()

        while True:
            try:
                resp = requests.get(self.sitename + request_url, auth=self.auth, verify=False) #verify=false necessary if running off server
                break
            except ConnectionError:
                if time.time() > start_time + connection_timeout:
                    raise Exception('Unable to get updates after {} seconds of ConnectionErrors'.format(connection_timeout))
                else:
                    time.sleep(1) # attempting once every second
            
        return resp.json()

    #As it stands now there are so few cases that it makes sense to just pull all of them each time. Should this
    #this change incorporate the script from pullAllDeskCases to pull based off time
    #(would require some tweaking of update cases)

    # INPUT: request_url - endpoint of api to make call to
    def cycle_pages(self, request_url, fname, fields=None):
        data = self.get_data(request_url)
    
        #need to use count and change url bc you can only read 500 pages per request, this way we change the date to be the last value and count upward
        #count starts at 1 because the first ever call counts as one, with other requests start at 0
        countLimit = 250
        count = 1
        totalCount = 1
        request_url = data['_links']['next']['href']
        print(data['total_entries'])
        first=True
        while (('_links' in data) and ('_embedded' in data) and (data['_links']['next'] != None)):
            if fields != None:
                try:
                    self.get_df(data['_embedded']['entries'], first, fname, fields=fields)
                except:
                    print('Fields Invalid, pulling default fields: ')
                    self.get_df(data['_embedded']['entries'], first, fname)
            else:
                self.get_df(data['_embedded']['entries'], first, fname)

            if (first):
                first=False

            request_url = request_url.replace("per_page=50&", "per_page=100&")
            request_url = request_url.replace("&per_page=50", "&per_page=100")
            #For unknown reasons, when authenticating via OAuth1, ':' is replaced by '%3A' -- reverses that
            if '%3A' in request_url:
                request_url = request_url.replace('%3A', ':')
            data= self.get_data(request_url)
            #When we reach the count limit we start a new request 
            count = count + 1
            totalCount = totalCount + 1
            print(str(count) + " - " + str(totalCount))
            if count is countLimit:
                # at the count limit we start a new request, the request uses the time of the last entrie in the previous call
                #the call needs to be in epoch format so we convert it and create the new request url
                n = (len(data['_embedded']['entries']) - 1)
                stime = data['_embedded']['entries'][n]['created_at'].replace("T", " ").replace("Z", "")
                ut = time.mktime(datetime.datetime.strptime(stime, "%Y-%m-%d %H:%M:%S").timetuple())
                request_url = "/api/v2/cases/search?since_created_at=" + str(ut)[:-2] + "&sort_direction=des&sort_field=since_created_at"
                count = 0
            else:
                if (data != None and data['_links']['next'] != None):
                    #if it is not at the count limit we change the request ulr to the next page and continue
                    request_url = data['_links']['next']['href']
            print("-----")

        if '_embedded' in data:
            #this is for the LAST call, it will not have a next link but will still have data so we will read it
            if fields != None:
                try:
                    self.get_df(data['_embedded']['entries'], first, fname, fields=fields)
                except:
                    print('Fields Invalid, pulling default fields: ')
                    self.get_df(data['_embedded']['entries'], first, fname)
                    print(str(self.fields))
            else:
                self.get_df(data['_embedded']['entries'], first, fname)
        else:
            print("no _embedded at end")

    #Grabs the JSON data and returns a properly formatted pandas dataframe with only the columns you specify

    # INPUT: data - dataset to select fields from
    # INPUT: fields - columns to be selected from dataset (add more if you need but for the most part this is all we should need)
    def get_df(self, data, first, fname, fields=['id', 'blurb', 'labels', 'label_ids', 'created_at', 'opened_at', 'resolved_at', 'received_at', 'updated_at','subject', '_links', 'status', 'custom_fields','type']):

                
        self.fields = fields
        json_data = json.dumps(data)

        for chunk in pd.read_json(json_data, chunksize=10000, lines=True):
            c = chunk.T
            c.rename(columns = {list(c)[0]: 'data'}, inplace = True)
            df = pd.DataFrame(c['data'].tolist())
            df_functional = df[:][fields].copy()
            if (first):
                first = False
                with open(fname, 'w') as outfile:
                    df_functional.to_csv(outfile)
            else:
                with open(fname, 'a') as outfile:
                    df_functional.to_csv(outfile)
    

    # #Writes the pandas data frame to a csv file

    # # INPUT: fname - name of csv to save data to
    # # INPUT: fname - name of csv to save data to
    # def load_csv(self, fname):
    #     print("load")
    #     with open(fname, 'w') as outfile:
    #         self.df.to_csv(outfile, index=False, chunksize=10000)

    #Combines cycle_pages, get_df, and load_csv into one function
    def pull_save_cases(self, request_url=None, fname=None, fields=None):
        try:
            print("Trying to pull cases from Desk.com")
            print(fname)
            self.cycle_pages(request_url, fname, fields=fields)
            print('You have successfully saved')
        except Exception as e: 
            print(e)
            print(e.__class__)
 
####################################
#   DELETE AND RECREATE DATABASE   #
####################################

def del_recreate_bq(projectId, datasetId, tableId, tableDescription):

    #https://cloud.google.com/bigquery/docs/reference/rest/v2/tables/

    bigquery = GoogleApi('bigquery', 'v2', [
        'https://www.googleapis.com/auth/bigquery',
        'https://www.googleapis.com/auth/devstorage.full_control'
    ])

    #NEED TO UPDATE TO YOUR JSON  KEY
    bigquery.with_service_account_file('Midd-d3b73b87b0cf.json')
    try:
        service_request = bigquery.tables().delete(projectId=projectId, datasetId=datasetId, tableId=tableId)
        service_request.execute()
    except:
        print('No table to delete')

    body = {
      "tableReference":
      {
        "projectId": projectId,
        "datasetId": datasetId,
        "tableId": tableId
      },
      "description": tableDescription
    }

    try:
        service_request = bigquery.tables().insert(projectId=projectId, datasetId=datasetId, body=body)
        service_request.execute()
        print('Success, created table ' + tableId)
    except:
        print('Unable to create table ' + tableId)
    return None

####################################
#   LOAD DATA TO BIGQUERY          #
####################################   

#this section is called last, it pulls the full dataset from the csv created earlier
#it then uploads the data to the bigquery database

#Modified from google documentation
def load_data_from_file(dataset_id, table_id, source_file_name):
    
    #this print full datafram if you need it to debug
    #pd.set_option('display.max_columns', None) 

    # Instantiates a client
    bigquery_client = bigquery.client.Client()
    dataset_ref = bigquery_client.dataset(dataset_id)
    table_ref = dataset_ref.table(table_id)

    # need this to specifiy what value to identify unique values by
    if (source_file_name == 'deskCasesLabels.csv'):
        drop_val = 'name'
    else:
        drop_val = 'id'

    # load clean, and upload the data to bq 10000 at a time
    cc = 0
    chunks = pd.read_csv(source_file_name, chunksize=10000, lineterminator='\n')

    try:
        for chunk in chunks:
            df = chunk.drop_duplicates([drop_val], keep='last')
            if 'Unnamed: 0' in list(df):
                df = df.drop('Unnamed: 0', axis=1)
            if (source_file_name != 'deskCasesLabels.csv'):
                values = {'id': 0, 'blurb':"",'labels':"[]", 'label_ids':"[]", 'created_at': "", 'opened_at': "", 'resolved_at': "", 'received_at': "", 'updated_at': "", 'subject': "", "_links":"",'status':"", 'custom_fields': "",'type':""}
                df.fillna(value=values)
                df.rename(columns={'labels':'assigned_labels'}, inplace=True)

            #need to do this because sometimes it can interpret the id as a float or time as string and it will break the upload, do this after dropna
            if (source_file_name != 'deskCasesLabels.csv'):
                df.id = df.id.astype(str)
                df.blurb = df.blurb.astype(str)
                df.assigned_labels = df.assigned_labels.astype(str)
                df.label_ids = df.label_ids.astype(str)

                df.created_at = pd.to_datetime(df['created_at'], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                df.opened_at = pd.to_datetime(df['opened_at'], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                df.resolved_at = pd.to_datetime(df['resolved_at'], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                df.received_at = pd.to_datetime(df['received_at'], format='%Y-%m-%d %H:%M:%S', errors='coerce')
                df.updated_at = pd.to_datetime(df['updated_at'], format='%Y-%m-%d %H:%M:%S', errors='coerce')

                df.subject = df.subject.astype(str)
                df._links = df._links.astype(str)
                df.status = df.status.astype(str)
                df.custom_fields = df.custom_fields.astype(str)
                df.type = df.type.astype(str)



            # This example uses CSV, but you can use other formats.
            # See https://cloud.google.com/bigquery/loading-data
            # https://google-cloud-python.readthedocs.io/en/latest/bigquery/generated/google.cloud.bigquery.client.Client.html#google.cloud.bigquery.client.Client
            job_config = bigquery.LoadJobConfig()
            job_config.source_format = 'Parquet'
            if (source_file_name == 'deskCasesLabels.csv'):
                job_config.schema = [
                   bigquery.SchemaField('name', 'STRING', description="Categories applied by Audience Relations that describe the email")
                ]
            else:
                #if a row of column labels is in dataframe remove it
                df['id'] = df['id'].map(lambda x: x.strip().lstrip().rstrip())
                df = df.loc[df['id'] != 'id']
  
                job_config.schema = [
                    bigquery.SchemaField('id', 'STRING', description="Unique Id of the email"),
                    bigquery.SchemaField('blurb', 'STRING', description="First 250 chars of the body of the email"),
                    bigquery.SchemaField('assigned_labels', 'STRING', description="Categories applied by Audience Relations that describe the email"),
                    bigquery.SchemaField('label_ids', 'STRING', description="Id's in desk for each Label used"),
                    bigquery.SchemaField('created_at', 'TIMESTAMP', description="When the case was created in desk (The email was sent to us)"),
                    bigquery.SchemaField('opened_at', 'TIMESTAMP', description="When the case was first opened by an agent"),
                    bigquery.SchemaField('resolved_at', 'TIMESTAMP', description="When the case was last resolved (If it has been)"),
                    bigquery.SchemaField('received_at', 'TIMESTAMP', description="The last time we recieved something for this case (Can be part of a chain of corespondence)"),
                    bigquery.SchemaField('updated_at', 'TIMESTAMP', description="Categories applied by Audience Relations that describe the email"),
                    bigquery.SchemaField('subject', 'STRING', description="A broad single category for the case (What the user self selected the email was about)"),
                    bigquery.SchemaField('_links', 'STRING', description="Various links in the API that relate to the case (attachments, customer, ect)"),
                    bigquery.SchemaField('status', 'STRING', description="Can be an open or resolved case (Have we dealt with the issue)"),
                    bigquery.SchemaField('custom_fields', 'STRING', description="A list of our (NPR's) custom fields for the case"),
                    bigquery.SchemaField('type', 'STRING', description="Type of case (almost always an email)")
                ]

            job = bigquery_client.load_table_from_dataframe(
            df, table_ref, job_config=job_config)

             # Waits for job to complete
            while True:
                try:
                    job.result()
                finally:
                    if (job.errors != 'None'):
                        print(job.errors)
                    if (job.error_result != 'None'):
                        print(job.error_result)
                if job.state == 'DONE':
                    if job.error_result:
                        raise RuntimeError(job.errors)
                    break
                time.sleep(1)

            print('Loaded {} rows into {}:{}.'.format(
            job.output_rows, dataset_id, table_id))

    except Exception as e:
        print("exception in load:")
        exc_type, exc_obj, exc_tb = sys.exc_info()
        fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]
        print(exc_type, fname, exc_tb.tb_lineno)


######################################
#   FUNCTIONS CALLED BELOW HERE      #
######################################

"""Can take configuration file with data for OAuth1, or else set yourself
If setting yourself, you can just pass in a username and password as such
import getpass
username = input('Enter username:')
password = getpass.getpass()
auth = username, password
"""
sitename = 'https://help.npr.desk.com'

npr_app_data = 'deskCasesNPRApp.csv'
labels_data = 'deskCasesLabels.csv'
last_month_data = 'deskCasesLastMonth.csv'

#https://docs.python.org/3/library/configparser.html
config = configparser.ConfigParser(allow_no_value=True)
config.read('desk_OAuth1.ini')
auth = OAuth1(config['DESK']['app_key'],
              config['DESK']['app_secret'],
              config['DESK']['oauth_token'],
              config['DESK']['oauth_token_secret'])

##########Change for Dataset#########
projectId = "midd-194719"
datasetId = "hippo"

tableIdNPRApp = "deskCasesNPRApp"
tableIdLabels = "deskLabels"
tableIdLastMonth = 'deskCasesLastMonth'

npr_app_data = 'deskCasesNPRApp.csv'
labels_data = 'deskCasesLabels.csv'
last_month_data = 'deskCasesLastMonth.csv'

app_desc = "Desk cases submitted through the feedback form in the app"
labels_desc = "A list of all labels ever applied to a case by audience relations"
last_month_desc = 'All cases in Desk from the last month'
#####################################

# #Has to Be imported beforehand
from google.cloud import bigquery


######################################
#   FUNCTIONS CALLED BELOW HERE      #
######################################

##### Grabing the desk cases for the NPR App #######################################################

#create desk client
client = Desk(sitename, auth)

### pull npr app cases
client.pull_save_cases(request_url="/api/v2/cases/search?q=custom_new_app:true&per_page=100&page=1", fname=npr_app_data)
#Deleteing and recreating the bq tables
del_recreate_bq(projectId, datasetId, tableIdNPRApp, app_desc)
#Loading data from the NPR App and Labels to their tables
load_data_from_file(datasetId, tableIdNPRApp, npr_app_data)

##### #Grabing updated list of all labels used in Desk ##################################
client.pull_save_cases(request_url="/api/v2/labels", fname=labels_data, fields=['name'])
#Deleteing and recreating the bq tables
del_recreate_bq(projectId, datasetId, tableIdLabels, labels_desc)
#Loading data from the NPR App and Labels to their tables
load_data_from_file(datasetId, tableIdLabels, labels_data)


###### Grabing last month of desk cases #######################################
client.pull_save_cases(request_url="/api/v2/cases/search?created=month", fname=last_month_data)
#Deleteing and recreating the bq tables
del_recreate_bq(projectId, datasetId, tableIdLastMonth, last_month_desc)
#Loading data from the NPR App and Labels to their tables
load_data_from_file(datasetId, tableIdLastMonth, last_month_data)
































#################################################################################################################################################################
# Here is some code i was using to clean dates while pulling cases before I saved them, i dont think i need it but it may be helpfull so i will save it for now #
#################################################################################################################################################################


# #here we validate if a date time is in the correct format, if it is we return it, if it is not we convert it or return a 
# #signal value so that row can be removed later

# # INPUT: data_text - object to validate
# def validate(date_text):

#     if isinstance(date_text, str):
#         try:
#             datetime.datetime.strptime(date_text, '%Y-%m-%dT%H:%M:%SZ')
#             datetimeobject = datetime.datetime.strptime(date_text,'%Y-%m-%dT%H:%M:%SZ')
#             newformat = datetimeobject.strftime('%Y-%m-%d %H-%M-%S')
#             ts = pd.to_datetime(newformat)
#             return ts
#         except ValueError:
#             # Fake Time so we can remove later
#             return pd.Timestamp('19001213 11:59:58')
#     else:
#         if isinstance(date_text, pd.Timestamp):
#             return date_text
#         else:
#             # Fake Time so we can remove later
#             return pd.Timestamp('19001213 11:59:58')            

# #cleans data, has to be a date type now unless you make different validate functions

# # INPUT: col_name - name of (date) column that you want to clean
# def clean_data(col_name, df):

#     before = len(df)
#     time = []

#     # go through every datetime and call validate on them
#     for c in df[col_name]:
        
#         g = validate(c)
#         time.append(g)

#     # drop and re add the field, with the new values
#     df.drop(col_name, axis = 1)
#     df[col_name] = time
#     #remove rows with signal values
#     df = df.loc[df[col_name] != pd.Timestamp('19001213 11:59:58')]
#     after = len(df)

#     print("before " + str(before))
#     print("after " + str(after))

#     return df









